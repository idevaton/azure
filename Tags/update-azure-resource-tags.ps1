# Before running this command login to Azure by running Connect-AzAccount
# if connect command is not found, run Install-Module Az as administrator

# Example TAGS:
# Environment
# Department
# Resource Type

$debugOn = 0 # to hide debug statements change this value to 0

if($debugOn -eq 1) {
	echo '************ Start Script ************'
}

# get all azure resources and loop through each one
foreach ($resource in $(Get-AzResource)) {
	
	if($debugOn -eq 1) {
		#output resource info
		echo '--- resource start ---'
		echo $resource echo
		echo '--- resource end ---'
	}

	# get resource properties
	$resourceName = $resource.Name
	$resourceType = $resource.ResourceType
	$resourceId = $resource.ResourceId
	$resourceTags = $resource.Tags
	
	# parse out resource type name
	$resourceTypeArray = $resourceType.Split('/')
	$resourceTypeName = $resourceTypeArray[0].replace('Microsoft.', '')

	# check if any tags are present
	if($resourceTags -ne $null) {

		# check which tags are missing
		foreach ($Tags in $resourceTags) {
			foreach ($Tag in $Tags) {
				#-----------------------------------------------------
				#-----------------------------------------------------
				$envTagValue = $Tag['Environment']
				if ($envTagValue -ne $null) {
					if($debugOn -eq 1) {
						Write-Output "Environment: $envTagValue"
					}
				}
				else
				{
					if ($debugOn -eq 1) {
						Write-Output "Environment tag is missing for: $resourceName type: $resourceType id: $resourceld"
					}
					# add missing tag
					# important: update command below is case sensitive and operation value is important
					$newTag = @{"Environment"="Unspecified"}
					Update-AzTag -Tag $newTag -Resourceld $resourceld -Operation Merge
				}
				#-----------------------------------------------------
				#-----------------------------------------------------
				$departmentTagValue = $Tag['Department']
				if ($departmentTagValue -ne $null) {
					if($debugOn -eq 1) {
						Write-Output "Department: $departmentTagValue"
					}
					# set default to IT
					if ($departmentTagValue -ne 'IT' ) {
						$newTag = @{"Department"="IT"}
						Update-AzTag -Tag $newTag -Resourceld $resourceld -Operation Merge
					}
				}
				else
				{
					if ($debugOn -eq 1) {
						Write-Output "Department tag is missing for: $resourceName type: $resourceType id: $resourceld"
					}
					# add missing tag
					# important: update command below is case sensitive and operation value is important
					$newTag = @{"Department"="IT"}
					Update-AzTag -Tag $newTag -Resourceld $resourceld -Operation Merge
				}
				#-----------------------------------------------------
				#-----------------------------------------------------
				$rtTagValue = $Tag['Resource Type']
				if ($rtTagValue -ne $null) {
					if($debugOn -eq 1) {
						Write-Output "Resource Type: $rtTagValue"
					}
					# set default to proper resource type name
					if ($rtTagValue -ne $resourceTypeName ) {
						$newTag = @{"Resource Type"=$resourceTypeName}
						Update-AzTag -Tag $newTag -Resourceld $resourceld -Operation Merge
					}
				}
				else
				{
					if ($debugOn -eq 1) {
						Write-Output "Resource Type tag is missing for: $resourceName type: $resourceType id: $resourceld"
					}
					# add missing tag
					# important: update command below is case sensitive and operation value is important
					$newTag = @{"Resource Type"=$resourceTypeName}
					Update-AzTag -Tag $newTag -Resourceld $resourceld -Operation Merge
				}
			}
		}
	}
	else {
		# resource did not have any tags, add missing tags
		if ($debugOn -eq 1) {
			write-Output "All tags are missing for: $resourceName type: $resourceType id: $resourceId"
		}
		$newTags = @{"Environment"="Unspecified"; "Department"="IT"; "Resource Type"=$resourceTypeName}
		New-AzTag -Resourceld $resourceId -Tag $newTags
	}
}

if($debugOn -eq 1) {
	echo '************ End Script ************'
}
